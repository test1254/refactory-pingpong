FROM node:11-alpine

# Create app directory
WORKDIR /usr/src/app

COPY package*.json ./
COPY yarn.lock ./

RUN yarn install
# Bundle app source
COPY . .

EXPOSE 3000
CMD yarn run start